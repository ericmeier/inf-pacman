import { ImageUtils, Pool, SpriteSheet } from "gtp";
import { Node } from "./Node";
import { GameRoot } from "../Game";

declare let game: GameRoot;

const dotPoints: number[] = [50, 10];

export class Level {
    private _data: number[][];
    private _levelCanvas: HTMLCanvasElement;
    private _eatenDotCount: number;
    private _dotCount: number;
    private _originalLevelInfo: number[][];

    public static readonly fruitDotCount = 64;
    public static readonly horizontalTileCount = 28;
    public static readonly verticalTileCount = 32;
    public static readonly bigDotTitle = 254;
    public static readonly smallDotTitle = 255;

    closed: Node[] = [];
    open: Node[] = [];
    goalNode: Node = new Node();

    private _nodeCache: Pool<Node>;

    constructor(levelInfo: number[][]) {
        this._data = [];
        this.reset(levelInfo);
    }

    private static _cloneObjectOfPrimitives(obj: any): any {
        return JSON.parse(JSON.stringify(obj));
    }

    checkForDot(row: number, col: number): number {
        let score: number = 0;
        const tile: number = this._getTileAt(row, col);

        if (tile >= 254) {
            game.playChompAudio();
            if (tile === 254) {
                game.makeGhostsBlue();
            }
            this._eatenDotCount++;
            this._data[row][col] = 0;
            score = dotPoints[tile - 254];
            if (this._eatenDotCount === Level.fruitDotCount) {
                game.addFruit();
            }
            if (this._eatenDotCount === this._dotCount) {
                game.loadNextStage();
            }
        }

        return score;
    }

    private static _constructPath(node: Node): Node {
        let prev: Node = null;
        while (node.parent) {
            prev = node;
            node = node.parent;
        }
        return prev;
    }

    private static _getNextColumn(col: number): number {
        if (++col === Level.horizontalTileCount) {
            col = 0;
        }
        return col;
    }

    getBreadthFirstPath(startRow: number, startColumn: number, endRow: number, endColumn: number): Node {
        this.open.forEach((node: Node) => {
            this._data[node.row][node.col] &= 255;
        });
        this.closed.forEach((node: Node) => {
            this._data[node.row][node.col] &= 255;
        });

        this.open.length = 0;
        this.closed.length = 0;
        this.goalNode.set(endRow, endColumn, null);
        let temp: Node = this._nodeCache.borrowObj();
        this.open.push(new Node(startRow, startColumn));
        this._data[startRow][startColumn] |= 256;

        while (this.open.length > 0) {
            const node: Node = this.open.splice(0, 1)[0];
            if (node.equals(this.goalNode)) {
                this._data[node.row][node.col] &= 255;
                return Level._constructPath(node);
            } else {
                this.closed.push(node);

                if (this.isWalkable(node.row - 1, node.col)) {
                    if ((this._data[node.row - 1][node.col] & 256) === 0) {
                        this._data[node.row - 1][node.col] |= 256;
                        temp.set(node.row - 1, node.col, node);
                        this.open.push(temp);
                        temp = this._nodeCache.borrowObj();
                    }
                }

                if (this.isWalkable(node.row + 1, node.col)) {
                    if ((this._data[node.row + 1][node.col] & 256) === 0) {
                        this._data[node.row + 1][node.col] |= 256;
                        temp.set(node.row + 1, node.col, node);
                        this.open.push(temp);
                        temp = this._nodeCache.borrowObj();
                    }
                }

                let col: number = Level._getPreviousColumn(node.col);
                if (this.isWalkable(node.row, col)) {
                    if ((this._data[node.row][col] & 256) === 0) {
                        this._data[node.row][col] |= 256;
                        temp.set(node.row, col, node);
                        this.open.push(temp);
                        temp = this._nodeCache.borrowObj();
                    }
                }

                col = Level._getNextColumn(node.col);
                if (this.isWalkable(node.row, col)) {
                    if ((this._data[node.row][col] & 256) === 0) {
                        this._data[node.row][col] |= 256;
                        temp.set(node.row, col, node);
                        this.open.push(temp);
                        temp = this._nodeCache.borrowObj();
                    }
                }
            }
        }

        // No Path found
        return undefined;
    }

    private static _getPreviousColumn(col: number): number {
        if (col === 0) {
            col = Level.horizontalTileCount;
        }
        return col - 1;
    }

    private _getTileAt(row: number, col: number): number {
        if (col < 0 || col >= Level.horizontalTileCount) {
            return -1;
        }
        if (row < 0 || row >= Level.verticalTileCount) {
            return -1;
        }
        return this._data[row][col] & 255;
    }

    isShotColumnClear(col: number, row1: number, row2: number): boolean {
        const start: number = Math.min(row1, row2);
        const end: number = Math.max(row1, row2);
        for (let i: number = start + 1; i < end; i++) {
            if (!this.isWalkable(i, col)) {
                return false;
            }
        }
        return true;
    }

    isShotRowClear(row: number, col1: number, col2: number): boolean {
        const start: number = Math.min(col1, col2);
        const end: number = Math.max(col1, col2);
        for (let i: number = start + 1; i < end; i++) {
            if (!this.isWalkable(row, i)) {
                return false;
            }
        }
        return true;
    }

    isWalkable(row: number, col: number): boolean {
        const tile: number = this._getTileAt(row, col);
        return tile === 0 || tile >= 240;
    }

    render(ctx: CanvasRenderingContext2D) {
        ctx.drawImage(this._levelCanvas, 0, 0);

        ctx.fillStyle = "#ffffff";
        for (let row: number = 0; row < Level.verticalTileCount; row++) {
            const y: number = row * 8 + 2 * 8;

            for (let col: number = 0; col < Level.horizontalTileCount; col++) {
                const tile: number = this._getTileAt(row, col);
                const x: number = col * 8;

                if (tile === Level.smallDotTitle) {
                    game.drawSmallDot(x + 3, y + 2);
                } else if (tile === Level.bigDotTitle) {
                    game.drawBigDot(x, y);
                }
            }
        }
    }

    reset(levelInfo?: number[][]) {
        "use strict";
        const firstTime: boolean = levelInfo != null;

        if (firstTime) {
            this._originalLevelInfo = Level._cloneObjectOfPrimitives(levelInfo);
        }

        this._data = Level._cloneObjectOfPrimitives(this._originalLevelInfo);
        this._eatenDotCount = 0;

        if (firstTime) {
            const mapTiles: SpriteSheet = game.assets.get("mapTiles");

            const levelY: number = 2 * 8;
            this._levelCanvas = ImageUtils.createCanvas(game.getWidth(), game.getHeight());
            const levelCtx: CanvasRenderingContext2D = this._levelCanvas.getContext("2d");
            let walkableCount: number = 0;
            this._dotCount = 0;

            levelCtx.fillStyle = "#000000";
            levelCtx.fillRect(0, 0, this._levelCanvas.width, this._levelCanvas.height);

            game.drawScoresHeaders(levelCtx);

            for (let row: number = 0; row < this._data.length; row++) {
                const rowData: number[] = this._data[row];

                for (let col: number = 0; col < rowData.length; col++) {
                    let tile: number = rowData[col];
                    if (tile === 0 || tile >= 240) {
                        walkableCount++;
                    }

                    switch (tile) {
                        case Level.smallDotTitle:
                        case Level.bigDotTitle:
                            this._dotCount++;
                            break;

                        default:
                            tile--;
                            if (tile > -1) {
                                const dx: number = col * 8;
                                const dy: number = levelY + row * 8;
                                mapTiles.drawByIndex(levelCtx, dx, dy, tile);
                            }
                            break;
                    }
                }
            }

            if (!this._nodeCache) {
                this._nodeCache = new Pool(Node, walkableCount);
            }
        }
    }
}
