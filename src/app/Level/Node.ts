export class Node {
    row: number;
    col: number;
    parent: Node;

    constructor(row: number = 0, col: number = 0) {
        this.row = row;
        this.col = col;
    }

    equals(node2: Node) {
        return this.row === node2.row && this.col === node2.col;
    }

    set(row: number, col: number, parent: Node) {
        this.row = row;
        this.col = col;
        this.parent = parent;
    }

    toString() {
        return "[levelNode: (" + this.row + "," + this.col + ")]";
    }
}
