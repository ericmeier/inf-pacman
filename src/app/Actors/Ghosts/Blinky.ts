import { Ghost, MovementState } from "./Ghost";
import { GameRoot } from "../../Game";
import { Direction } from "../Actor";
import { Level } from "../../Level/Level";
import { Node } from "../../Level/Node";

export class Blinky extends Ghost {
    constructor(game: GameRoot) {
        super(game, 0 * 16, 0);
    }

    reset() {
        super.reset();
        this.direction = Direction.Right;
        this.setLocation(this.game.penaltyBoxExitX, this.game.penaltyBoxExitY);
        this.movmentState = MovementState.Scattering;
    }

    updatePacmanChasingPosition(level: Level) {
        const movementDelta: number = this.movementDelta;

        if (this.handleIntersection(level)) {
            const startRow: number = this.row;
            const startColumn: number = this.column;
            const endRow: number = this.game.pacman.row;
            const endColumn: number = this.game.pacman.column;
            const node: Node = level.getBreadthFirstPath(startRow, startColumn, endRow, endColumn);

            if (node == null) {
                this.changeDirection(level);
            } else if (node.col < startColumn) {
                this.direction = Direction.Right;
                this.increaseX(-movementDelta);
            } else if (node.col > startColumn) {
                this.direction = Direction.Left;
                this.increaseX(movementDelta);
            } else if (node.row < startRow) {
                this.direction = Direction.Down;
                this.increaseY(-movementDelta);
            } else if (node.row > startRow) {
                this.direction = Direction.Up;
                this.increaseY(movementDelta);
            }
        } else {
            this.continueInDirection(movementDelta);
        }

        if (this.game.playTime >= this.startScatteringTime) {
            this.movmentState = MovementState.Scattering;
        }
    }
}
