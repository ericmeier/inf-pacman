import { Ghost, MovementState } from "./Ghost";
import { GameRoot } from "../../Game";
import { Direction } from "../Actor";
import { Level } from "../../Level/Level";
import { Node } from "../../Level/Node";

declare let game: GameRoot;

export class Inky extends Ghost {
    constructor(game: GameRoot) {
        super(game, 1 * 16, 8);
    }

    reset() {
        super.reset();
        this.direction = Direction.Up;
        this.setLocation(
            12 * 8 - 8 / 2 - 4,
            15 * 8 - 8 / 2
        );
        this.movmentState = MovementState.InBox;
    }

    updatePacmanChasingPosition(level: Level) {
        const movementDelta: number = this.movementDelta;

        if (this.handleIntersection(level)) {
            const row: number = this.row;
            const col: number = this.column;
            const blinky: Ghost = game.getGhost(0);
            const blinkyRow: number = blinky.row;
            const blinkyColumn: number = blinky.column;
            const distSq: number = (blinkyColumn - col) * (blinkyColumn - col) + (blinkyRow - row) * (blinkyRow - row);

            if (distSq <= 35) {
                const endRow: number = game.pacman.row;
                const endColumn: number = game.pacman.column;
                const node: Node = level.getBreadthFirstPath(row, col, endRow, endColumn);
                if (node == null) {
                    this.changeDirection(level);
                } else if (node.col < col) {
                    this.direction = Direction.Right;
                    this.increaseX(-movementDelta);
                } else if (node.col > col) {
                    this.direction = Direction.Left;
                    this.increaseX(movementDelta);
                } else if (node.row < row) {
                    this.direction = Direction.Down;
                    this.increaseY(-movementDelta);
                } else if (node.row > row) {
                    this.direction = Direction.Up;
                    this.increaseY(movementDelta);
                }
            } else {
                this.changeDirection(level);
            }
        } else {
            this.continueInDirection(movementDelta);
        }
        if (game.playTime >= this.startScatteringTime) {
            this.movmentState = MovementState.Scattering;
        }
    }
}
