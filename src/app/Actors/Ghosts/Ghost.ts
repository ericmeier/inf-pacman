import { Actor } from "../Actor";
import { Point, Utils } from "gtp";
import { Level } from "../../Level/Level";
import { Direction } from "../Actor";
import { GameRoot } from "../../Game";
import { Node } from "../../Level/Node";
import { Pacman } from "../Pacman/Pacman";

declare let game: GameRoot;

export enum MovementState {
    InBox = 0,
    LeavingBox = 1,
    Chasing = 2,
    Scattering = 3,
    Blue = 4,
    Eyes = 5,
    EnteringBox = 6
}

export abstract class Ghost extends Actor {
    game: GameRoot;

    private _movementState: MovementState;
    private readonly _corner: Point;
    private _scatterCount: number;
    private _exitScatteringTime: number;
    startScatteringTime: number;
    exitBlueTime: number;
    private _previousState: number;
    private readonly _spriteSheetY: number;
    private readonly _exitDelaySeconds: number;

    constructor(game: GameRoot, spriteSheetY: number, exitDelaySeconds: number) {
        super(2);
        this.game = game;
        this._corner = new Point();
        this._spriteSheetY = spriteSheetY;
        this._exitDelaySeconds = exitDelaySeconds;
        this.reset();
    }

    changeDirection(level: Level) {
        const movementDelta: number = this.movementDelta;
        const temp: number = Utils.randomInt(4);

        switch (temp) {
            case 0:
                if (!this.goUp(level, movementDelta)) {
                    if (!this.goLeft(level, movementDelta)) {
                        if (!this.goRight(level, movementDelta)) {
                            this.goDown(level, movementDelta);
                        }
                    }
                }
                break;

            case 1:
                if (!this.goLeft(level, movementDelta)) {
                    if (!this.goUp(level, movementDelta)) {
                        if (!this.goDown(level, movementDelta)) {
                            this.goRight(level, movementDelta);
                        }
                    }
                }
                break;

            case 2:
                if (!this.goDown(level, movementDelta)) {
                    if (!this.goLeft(level, movementDelta)) {
                        if (!this.goRight(level, movementDelta)) {
                            this.goUp(level, movementDelta);
                        }
                    }
                }
                break;

            case 3:
                if (!this.goRight(level, movementDelta)) {
                    if (!this.goUp(level, movementDelta)) {
                        if (!this.goDown(level, movementDelta)) {
                            this.goLeft(level, movementDelta);
                        }
                    }
                }
                break;
        }
    }

    continueInDirection(movementDelta: number) {
        switch (this.direction) {
            case Direction.Down:
                this.increaseY(-movementDelta);
                break;
            case Direction.Right:
                this.increaseX(-movementDelta);
                break;
            case Direction.Up:
                this.increaseY(movementDelta);
                break;
            case Direction.Left:
                this.increaseX(movementDelta);
                break;
        }
    }

    private _getStageBlueTime(stage: number): number {
        if (stage > 7) return 2000;

        if (stage > 4) return 4000;

        if (stage > 2) return 6000;

        return 8000;
    }

    private _getFirstExitDelay(): number {
        return this._exitDelaySeconds * 1000;
    }

    getFrameCount(): number {
        return 2;
    }

    get movmentState(): MovementState {
        return this._movementState;
    }

    getUpdateDelay(): number {
        switch (this._movementState) {
            case MovementState.Blue:
                return 25;
            case MovementState.Eyes:
            case MovementState.EnteringBox:
                return 10;
            default:
                return 10;
        }
    }

    isBlue(): boolean {
        return this._movementState === MovementState.Blue;
    }

    isEyes(): boolean {
        return this._movementState === MovementState.Eyes || this._movementState === MovementState.EnteringBox;
    }

    paint() {
        const destinationX: number = this.borders.x;
        const destinationY: number = this.borders.y;
        let srcX: number, srcY: number;

        switch (this._movementState) {
            case MovementState.Blue:
                srcX = (10 + this.getFrame()) * 16;
                srcY = 3 * 16;
                const playTime: number = game.playTime;
                if (this.exitBlueTime - playTime <= 1000) {
                    if (((playTime / 250) & 1) !== 0) {
                        srcY += 16;
                    }
                }
                game.drawSprite(destinationX, destinationY, srcX, srcY);
                break;

            case MovementState.Eyes:
            case MovementState.EnteringBox:
                srcX = this.direction * 16;
                srcY = 4 * 16;
                game.drawSprite(destinationX, destinationY, srcX, srcY);
                break;

            default:
                srcX = this.direction * 16 * this.getFrameCount() + this.getFrame() * 16;
                srcY = this._spriteSheetY;
                game.drawSprite(destinationX, destinationY, srcX, srcY);
                break;
        }
    }

    possiblyTurnBlue(): boolean {
        switch (this._movementState) {
            case MovementState.Chasing:
            case MovementState.Scattering:
            case MovementState.Blue:
                this.movmentState = MovementState.Blue;
                return true;
            default:
                return false;
        }
    }

    reset() {
        super.reset();
        this._scatterCount = 0;
    }

    setCorner(corner: Point) {
        this._corner.x = corner.x;
        this._corner.y = corner.y;
    }

    set movmentState(movementState: MovementState) {
        const game: GameRoot = this.game;

        if (movementState === MovementState.Scattering) {
            switch (this._scatterCount++) {
                case 0:
                case 1:
                    this._exitScatteringTime = game.playTime + 7000;
                    this._movementState = movementState;
                    break;
                case 2:
                case 3:
                    this._exitScatteringTime = game.playTime + 5000;
                    this._movementState = movementState;
                    break;
                default:
                    this._movementState = MovementState.Chasing;
                    break;
            }
        } else if (movementState === MovementState.Blue) {
            const blueTime: number = this._getStageBlueTime(game.stage);
            const playTime: number = game.playTime;
            this.exitBlueTime = playTime + blueTime;

            switch (this._movementState) {
                case MovementState.Chasing:
                    this._previousState = this._movementState;
                    this.startScatteringTime += blueTime;
                    break;
                case MovementState.Scattering:
                    this._previousState = this._movementState;
                    this._exitScatteringTime += blueTime;
                    break;
                case MovementState.Blue:
                    const prevBlueTimeRemaining: number = this.exitBlueTime - playTime;
                    switch (this._previousState) {
                        case MovementState.Chasing:
                            this.startScatteringTime += prevBlueTimeRemaining + blueTime;
                            break;
                        case MovementState.Scattering:
                            this._exitScatteringTime += prevBlueTimeRemaining + blueTime;
                            break;
                    }
                    break;
                default:
                    throw new Error("Unexpected state: " + this._movementState);
            }
            this._movementState = movementState;
        } else {
            if (this._movementState === MovementState.Chasing) {
                this.startScatteringTime = game.playTime + 20000;
            }
            this._movementState = movementState;
        }

        this.game.checkLoopedAudio();
    }

    _updateBluePosition(level: Level) {
        const movementDelta: number = this.movementDelta;
        if (this.handleIntersection(level)) {
            const pacman: Pacman = game.pacman;
            const pacRow: number = pacman.row;
            const pacColumn: number = pacman.column;
            const row: number = this.row;
            const col: number = this.column;
            let moved: boolean = false;

            if (row === pacRow && level.isShotRowClear(row, col, pacColumn)) {
                if (!this.goUp(level, movementDelta)) {
                    if (!this.goDown(level, movementDelta)) {
                        if (pacColumn < col) {
                            if (!this.goRight(level, movementDelta)) {
                                this.direction = Direction.Right;
                                this.increaseX(-movementDelta);
                            }
                        } else {
                            if (!this.goLeft(level, movementDelta)) {
                                this.direction = Direction.Left;
                                this.increaseX(movementDelta);
                            }
                        }
                    }
                }
                moved = true;
            } else if (col === pacColumn && level.isShotColumnClear(col, row, pacRow)) {
                if (!this.goLeft(level, movementDelta)) {
                    if (!this.goRight(level, movementDelta)) {
                        if (pacRow < row) {
                            if (!this.goDown(level, movementDelta)) {
                                this.direction = Direction.Down;
                                this.increaseY(-movementDelta);
                            }
                        } else {
                            if (!this.goUp(level, movementDelta)) {
                                this.direction = Direction.Up;
                                this.increaseY(movementDelta);
                            }
                        }
                    }
                }
                moved = true;
            }

            if (!moved) {
                this.changeDirection(level);
            }
        } else {
            this.continueInDirection(movementDelta);
        }

        if (game.playTime >= this.exitBlueTime) {
            this.movmentState = this._previousState;
        }
    }

    abstract updatePacmanChasingPosition(level: Level): void;

    private _updateEyePosition(level: Level) {
        const movementDelta: number = this.movementDelta;

        if (this.handleIntersection(level)) {
            const startRow: number = this.row;
            const startColumn: number = this.column;
            const endRow: number = Math.floor((game.penaltyBoxExitY + 8) / 8);
            let endColumn: number = Math.floor(game.penaltyBoxExitX / 8);
            if (startColumn <= endColumn) {
                endColumn++;
            }

            const node: Node = level.getBreadthFirstPath(startRow, startColumn, endRow, endColumn);
            if (node == null) {
                this.movmentState = MovementState.EnteringBox;
            } else {
                if (node.col < startColumn) {
                    this.direction = Direction.Right;
                    this.increaseX(-movementDelta);
                } else if (node.col > startColumn) {
                    this.direction = Direction.Left;
                    this.increaseX(movementDelta);
                } else if (node.row < startRow) {
                    this.direction = Direction.Down;
                    this.increaseY(-movementDelta);
                } else if (node.row > startRow) {
                    this.direction = Direction.Up;
                    this.increaseY(movementDelta);
                }
            }
        } else {
            const startRow: number = this.row;
            const endRow: number = Math.floor((game.penaltyBoxExitY + 8) / 8);

            if (startRow === endRow && this.x === game.penaltyBoxExitX) {
                this.movmentState = MovementState.EnteringBox;
            } else {
                this.continueInDirection(movementDelta);
            }
        }
    }

    private _updateEyesEnteringBoxPosition(level: Level) {
        const movementDelta: number = 0.5;

        const y: number = this.y;
        if (y < game.penaltyBoxExitY + (3 * 16) / 2) {
            this.direction = Direction.Up;
            this.increaseY(movementDelta);
        } else {
            this.movmentState = MovementState.LeavingBox;
        }
    }

    updatePositionImplicitly(level: Level) {
        switch (this._movementState) {
            case MovementState.InBox:
                this.updatePositionInBox(level);
                break;
            case MovementState.LeavingBox:
                this.updatePositionLeavingBox(level);
                break;
            case MovementState.Scattering:
                this.updatePositionScattering(level);
                break;
            case MovementState.Chasing:
                this.updatePacmanChasingPosition(level);
                break;
            case MovementState.Blue:
                this._updateBluePosition(level);
                break;
            case MovementState.Eyes:
                this._updateEyePosition(level);
                break;
            case MovementState.EnteringBox:
                this._updateEyesEnteringBoxPosition(level);
                break;
        }
    }

    updatePositionInBox(level: Level) {
        const movementDelta: number = 0.5;

        switch (this.direction) {
            case Direction.Right:
            case Direction.Down:
                if (this.y > 112) {
                    this.increaseY(-movementDelta);
                } else {
                    this.direction = Direction.Up;
                }
                break;
            case Direction.Left:
            case Direction.Up:
                if (this.y < 120) {
                    this.increaseY(movementDelta);
                } else {
                    this.direction = Direction.Down;
                }
                break;
        }

        if (game.playTime >= this._getFirstExitDelay()) {
            this.movmentState = MovementState.LeavingBox;
        }
    }

    updatePositionLeavingBox(level: Level) {
        const movementDelta: number = 0.5;

        const x: number = this.x;
        if (x < game.penaltyBoxExitX) {
            this.direction = Direction.Left;
            this.increaseX(movementDelta);
        } else if (x > game.penaltyBoxExitX) {
            this.direction = Direction.Right;
            this.increaseX(-movementDelta);
        } else {
            let y: number = this.y - movementDelta;
            this.y = y;
            if (y <= game.penaltyBoxExitY) {
                y = game.penaltyBoxExitY;
                this.movmentState = MovementState.Scattering;
                this.direction = Direction.Right;
            } else {
                this.direction = Direction.Down;
            }
        }
    }

    updatePositionScattering(level: Level) {
        const movementDelta: number = this.movementDelta;

        if (this.handleIntersection(level)) {
            const startRow: number = this.row;
            const startColumn: number = this.column;
            const endRow: number = this._corner.x;
            const endColumn: number = this._corner.y;
            const node: Node = level.getBreadthFirstPath(startRow, startColumn, endRow, endColumn);
            if (!node) {
                this.changeDirection(level);
            } else {
                if (node.col < startColumn) {
                    this.direction = Direction.Right;
                    this.increaseX(-movementDelta);
                } else if (node.col > startColumn) {
                    this.direction = Direction.Left;
                    this.increaseX(movementDelta);
                } else if (node.row < startRow) {
                    this.direction = Direction.Down;
                    this.increaseY(-movementDelta);
                } else if (node.row > startRow) {
                    this.direction = Direction.Up;
                    this.increaseY(movementDelta);
                }
            }
        } else {
            this.continueInDirection(movementDelta);
        }

        if (game.playTime >= this._exitScatteringTime) {
            this.movmentState = MovementState.Chasing;
        }
    }
}
