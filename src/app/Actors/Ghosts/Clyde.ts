import { Ghost, MovementState } from "./Ghost";
import { GameRoot } from "../../Game";
import { Direction } from "../Actor";
import { Level } from "../../Level/Level";

declare let game: GameRoot;

export class Clyde extends Ghost {
    constructor(game: GameRoot) {
        super(game, 3 * 16, 14);
    }

    reset() {
        super.reset();
        this.direction = Direction.Up;
        this.setLocation(
            16 * 8 - 8 / 2 - 4,
            15 * 8 - 8 / 2
        );
        this.movmentState = MovementState.InBox;
    }

    updatePacmanChasingPosition(level: Level) {
        const movementDelta: number = this.movementDelta;

        if (this.handleIntersection(level)) {
            this.changeDirection(level);
        } else {
            this.continueInDirection(movementDelta);
        }

        if (game.playTime >= this.startScatteringTime) {
            this.movmentState = MovementState.Scattering;
        }
    }
}
