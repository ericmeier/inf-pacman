import { Ghost, MovementState } from "./Ghost";
import { GameRoot } from "../../Game";
import { Direction } from "../Actor";
import { Level } from "../../Level/Level";
import { Pacman } from "../Pacman/Pacman";

declare let game: GameRoot;

export class Pinky extends Ghost {
    constructor(game: GameRoot) {
        super(game, 2 * 16, 2);
    }

    reset() {
        super.reset();
        this.direction = Direction.Down;
        this.setLocation(
            14 * 8 - 8 / 2 - 4,
            15 * 8 - 8 / 2
        );
        this.movmentState = MovementState.InBox;
    }

    updatePacmanChasingPosition(level: Level) {
        const movementDelta: number = this.movementDelta;

        const pacman: Pacman = game.pacman;
        const pacRow: number = pacman.row;
        const pacColumn: number = pacman.column;
        const row: number = this.row;
        const col: number = this.column;
        let moved: boolean = false;

        if (this.handleIntersection(level)) {
            if (row === pacRow && level.isShotRowClear(row, col, pacColumn)) {
                if (pacColumn < col) {
                    this.direction = Direction.Right;
                    this.increaseX(-movementDelta);
                } else {
                    if (!this.goRight(level, movementDelta)) {
                        this.changeDirection(level);
                    }
                }
                moved = true;
            } else if (col === pacColumn && level.isShotColumnClear(col, row, pacRow)) {
                if (pacRow < row) {
                    this.direction = Direction.Down;
                    this.increaseY(-movementDelta);
                } else {
                    if (!this.goDown(level, movementDelta)) {
                        this.changeDirection(level);
                    }
                }
                moved = true;
            }

            if (!moved) {
                this.changeDirection(level);
            }
        } else {
            this.continueInDirection(movementDelta);
        }
        if (game.playTime >= this.startScatteringTime) {
            this.movmentState = MovementState.Scattering;
        }
    }
}
