import { Level } from "../../Level/Level";
import { Direction } from "../Actor";
import { Actor } from "../Actor";
import { GameRoot } from "../../Game";
declare let game: GameRoot;

export class Pacman extends Actor {
    private _dyingFrame: number = 0;
    private _desiredDirection: Direction;

    constructor() {
        super(3);
    }

    getUpdateDelay(): number {
        return 10;
    }

    handleInput(level: Level) {
        if (game.inputManager.left()) this._desiredDirection = Direction.Right;
        else if (game.inputManager.right()) this._desiredDirection = Direction.Left;

        if (game.inputManager.up()) this._desiredDirection = Direction.Down;
        else if (game.inputManager.down()) this._desiredDirection = Direction.Up;
    }

    incDying(): boolean {
        this._dyingFrame = (this._dyingFrame + 1) % 12;
        return this._dyingFrame !== 0;
    }

    render(ctx: CanvasRenderingContext2D) {
        let srcX: number, srcY: number;
        if (this._dyingFrame > 0) {
            srcX = 16 * this._dyingFrame;
            srcY = 96;
        } else {
            srcX = this.direction * 16 * this.getFrameCount() + this.getFrame() * 16;
            srcY = 80;
        }

        game.drawSprite(this.borders.x, this.borders.y, srcX, srcY);
    }

    reset() {
        super.reset();
        this.direction = Direction.Right;
        this._desiredDirection = Direction.Left;
        this.setLocation(13 * 8, 24 * 8 - 8 / 2);
        this._frame = 0;
    }

    setLocation(x: number, y: number) {
        super.setLocation(x, y);
    }

    startDying() {
        this._dyingFrame = 1;
    }

    updatePositionImplicitly(level: Level) {
        if (this._desiredDirection == Direction.Right && this.getCanMoveLeft(level)) this.direction = Direction.Right;

        if (this._desiredDirection == Direction.Left && this.getCanMoveRight(level)) this.direction = Direction.Left;

        if (this._desiredDirection == Direction.Down && this.getCanMoveUp(level)) this.direction = Direction.Down;

        if (this._desiredDirection == Direction.Up && this.getCanMoveDown(level)) this.direction = Direction.Up;

        switch (this.direction) {
            case Direction.Right:
                this.goLeft(level, this.movementDelta);
                break;
            case Direction.Left:
                this.goRight(level, this.movementDelta);
                break;
            case Direction.Down:
                this.goUp(level, this.movementDelta);
                break;
            case Direction.Up:
                this.goDown(level, this.movementDelta);
                break;
        }

        game.increaseScore(level.checkForDot(this.row, this.column));
    }
}
