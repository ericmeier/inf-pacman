import { Actor } from "../Actor";
import { GameRoot } from "../../Game";
import { Level } from "../../Level/Level";

declare let game: GameRoot;

export class Fruit extends Actor {
    private readonly _row: number;
    private readonly _col: number;
    private readonly _pointsIndex: number;
    private static readonly columns: number[] = [12, 13, 12, 13, 13, 12, 13, 13];
    private static readonly rows: number[] = [4, 4, 5, 5, 2, 6, 6, 3];
    private static readonly pointsIndex: number[] = [0, 2, 4, 5, 10, 7, 9, 11];

    constructor() {
        super(1);

        this.setLocation(game.penaltyBoxExitX, 140);
        let stage: number = game.stage;
        if (stage > 7) {
            stage = game.randomInt(8);
        }

        this._col = Fruit.columns[stage];
        this._row = Fruit.rows[stage];
        this._pointsIndex = Fruit.pointsIndex[stage];
    }

    get pointsIndex(): number {
        return this._pointsIndex;
    }

    getUpdateDelay(): number {
        return 100000000000;
    }

    paint() {
        const srcX: number = this._col * 16;
        const srcY: number = this._row * 16;
        game.drawSprite(this.x, this.y, srcX, srcY);
    }

    updatePositionImplicitly(level: Level): void {}
}
