import { Rectangle } from "gtp";
import { Level } from "../Level/Level";

export enum Direction {
    Left = 0,
    Up = 1,
    Right = 2,
    Down = 3,
    None = 4
}

export abstract class Actor {
    borders: Rectangle;
    direction: Direction;
    _frame: number;

    private readonly _intersectBorders: Rectangle;
    private readonly _frameCount: number;
    private _lastUpdateTime: number;

    constructor(frameCount: number) {
        this.borders = new Rectangle(0, 0, 16, 16);
        this._intersectBorders = new Rectangle();
        this.direction = Direction.Left;
        this._frame = 0;
        this._frameCount = frameCount;
        this._lastUpdateTime = 0;
    }

    handleIntersection(level: Level): boolean {
        switch (this.direction) {
            case Direction.Down:
            case Direction.Up:
                return this.getCanMoveLeft(level) || this.getCanMoveRight(level);
            case Direction.Left:
            case Direction.Right:
                return this.getCanMoveUp(level) || this.getCanMoveDown(level);
        }
        return false;
    }

    getCanMoveDown(level: Level) {
        const x: number = this.centerX;
        const y: number = this.centerY;
        const xRemainder: number = x % 8;
        const yRemainder: number = y % 8;
        if (xRemainder === 0 && yRemainder === 0) {
            const row: number = this.row;
            const col: number = this.column;
            return row < 30 && level.isWalkable(row + 1, col);
        }
        return this.direction === Direction.Down || this.direction === Direction.Up;
    }

    getCanMoveLeft(level: Level) {
        let x: number = this.borders.x;
        if (x < 0) {
            return true;
        }
        x += 8 / 2;
        const y: number = this.centerY;
        const xRemainder: number = x % 8;
        const yRemainder: number = y % 8;
        if (xRemainder === 0 && yRemainder === 0) {
            const row: number = this.row;
            const col: number = this.column;
            return col > 0 && level.isWalkable(row, col - 1);
        }
        return this.direction === Direction.Left || this.direction === Direction.Right;
    }

    getCanMoveRight(level: Level) {
        let x: number = this.borders.x;
        if (x + this.width > this.screenWidth) {
            return true;
        }
        x += 8 / 2;
        const y: number = this.centerY;
        const xRemainder: number = x % 8;
        const yRemainder: number = y % 8;
        if (xRemainder === 0 && yRemainder === 0) {
            const row: number = this.row;
            const col: number = this.column;
            return col < 27 && level.isWalkable(row, col + 1);
        }
        return this.direction === Direction.Left || this.direction === Direction.Right;
    }

    getCanMoveUp(level: Level) {
        const x: number = this.centerX;
        const y: number = this.centerY;
        const xRemainder: number = x % 8;
        const yRemainder: number = y % 8;
        if (xRemainder === 0 && yRemainder === 0) {
            const row: number = this.row;
            const col: number = this.column;
            return row > 0 && level.isWalkable(row - 1, col);
        }
        return this.direction === Direction.Down || this.direction === Direction.Up;
    }

    get centerX(): number {
        return this.borders.x + 8 / 2;
    }

    get centerY(): number {
        return this.borders.y + 8 / 2;
    }

    get column(): number {
        let col: number = Math.floor(this.centerX / 8);

        if (col < 0) {
            col += Level.horizontalTileCount;
        } else if (col >= Level.horizontalTileCount) {
            col -= Level.horizontalTileCount;
        }

        return col;
    }

    getFrame() {
        return this._frame;
    }

    getFrameCount() {
        return this._frameCount;
    }

    private get intersectBorders(): Rectangle {
        this._intersectBorders.set(this.borders.x + 2, this.borders.y - 2, this.borders.w - 4, this.borders.h - 4);
        return this._intersectBorders;
    }

    get movementDelta(): number {
        return 1;
    }

    get 8(): number {
        return 8;
    }

    get width(): number {
        return this.borders.w;
    }

    get row(): number {
        return Math.floor(this.centerY / 8);
    }

    abstract getUpdateDelay(): number;

    get x(): number {
        return this.borders.x;
    }

    get y(): number {
        return this.borders.y;
    }

    goDown(level: Level, movementDelta: number): boolean {
        if (this.getCanMoveDown(level)) {
            this.direction = Direction.Up;
            this.increaseY(movementDelta);
            return true;
        }
        return false;
    }

    goLeft(level: Level, movementDelta: number): boolean {
        if (this.getCanMoveLeft(level)) {
            this.direction = Direction.Right;
            this.increaseX(-movementDelta);
            return true;
        }
        return false;
    }

    goRight(level: Level, movementDelta: number): boolean {
        if (this.getCanMoveRight(level)) {
            this.direction = Direction.Left;
            this.increaseX(movementDelta);
            return true;
        }
        return false;
    }

    goUp(level: Level, movementDelta: number): boolean {
        if (this.getCanMoveUp(level)) {
            this.direction = Direction.Down;
            this.increaseY(-movementDelta);
            return true;
        }
        return false;
    }

    increaseX(delta: number) {
        this.borders.x += delta;
        if (this.borders.x + this.width <= 0) {
            this.borders.x += this.screenWidth;
        } else if (this.borders.x >= this.screenWidth) {
            this.borders.x -= this.screenWidth;
        }
    }

    increaseY(delta: number) {
        this.borders.y += delta;
    }

    intersects(sprite2: Actor): boolean {
        return this.intersectBorders.intersects(sprite2.intersectBorders);
    }

    reset() {
        this._lastUpdateTime = 0;
    }

    get screenWidth(): number {
        return 224;
    }

    setLocation(x: number, y: number) {
        this.borders.x = x;
        this.borders.y = y;
    }

    updateFrame() {
        this._frame = (this._frame + 1) % this.getFrameCount();
    }

    updatePosition(level: Level, time: number) {
        if (time > this._lastUpdateTime + this.getUpdateDelay()) {
            this._lastUpdateTime = time;
            this.updatePositionImplicitly(level);
        }
    }

    set x(x: number) {
        this.borders.x = x;
    }

    set y(y: number) {
        this.borders.y = y;
    }

    abstract updatePositionImplicitly(level: Level): void;
}
