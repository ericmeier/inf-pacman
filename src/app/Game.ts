import { Game, Image, Point, SpriteSheet, Utils } from "gtp";
import { Sounds } from "./Sounds";
import { Pacman } from "./Actors/Pacman/Pacman";
import { Fruit } from "./Actors/Items/Fruit";
import { Ghost } from "./Actors/Ghosts/Ghost";
import { Blinky } from "./Actors/Ghosts/Blinky";
import { Pinky } from "./Actors/Ghosts/Pinky";
import { Inky } from "./Actors/Ghosts/Inky";
import { Clyde } from "./Actors/Ghosts/Clyde";
import { GameScene } from "./Scenes/Game/Game";
import { Level } from "./Level/Level";

export enum GhostUpdateStrategy {
    UpdateAll,
    UpdateNone,
    UpdateOne
}

export class GameRoot extends Game {
    private readonly _ghosts: Ghost[];
    private readonly _extraPointsArray: number[];

    private _highScore: number;
    private _hearts: number;
    private _score: number;
    private _stage: number;
    private _ghostUpdateStrategy: GhostUpdateStrategy;
    private _chompAudio: number;
    private _fruit: Fruit;

    private _earnedExtraLife: boolean;
    private _loopedAudioId: number;
    private _loopedAudioName: string;
    private _resettingGhostStates: boolean;
    private _eatenGhostPointsIndex: number;
    private _fruitScoreEndTime: number;
    private _fruitScoreIndex: number;

    public pacman: Pacman;
    public static readonly extraLifeScore = 10000;
    public static readonly displayScoreLength = 750;

    constructor(args?: any) {
        super(args);
        this._highScore = 50000;
        this.pacman = new Pacman();
        this._ghosts = this._createGhostArray();
        this._chompAudio = 0;
        this._ghostUpdateStrategy = GhostUpdateStrategy.UpdateAll;
        this._score = 0;

        this._extraPointsArray = [100, 200, 300, 400, 500, 700, 800, 1000, 1600, 2000, 3000, 5000];
    }

    addFruit() {
        if (!this._fruit) {
            this._fruit = new Fruit();
            this._fruitScoreIndex = -1;
            this._fruitScoreEndTime = -1;
        }
    }

    checkForCollision(): Ghost {
        for (let i: number = 0; i < this._ghosts.length; i++) {
            if (this.pacman.intersects(this._ghosts[i])) {
                return this._ghosts[i];
            }
        }

        if (this._fruit && this._fruitScoreIndex === -1 && this.pacman.intersects(this._fruit)) {
            this.increaseScore(this._extraPointsArray[this._fruit.pointsIndex]);
            this.audio.playSound(Sounds.Fruit, false);
            this._fruitScoreIndex = this._fruit.pointsIndex;
            this._fruitScoreEndTime = this.playTime + GameRoot.displayScoreLength;
        }

        return null;
    }

    checkLoopedAudio() {
        if (this._resettingGhostStates) {
            return;
        }

        let blue: boolean = false;

        for (let i: number = 0; i < this._ghosts.length; i++) {
            if (this._ghosts[i].isEyes()) {
                this.setLoopedAudio(Sounds.RunningEyes);
                return;
            } else if (this._ghosts[i].isBlue()) blue = true;
        }

        this.setLoopedAudio(blue ? Sounds.Chasing : Sounds.Siren);
    }

    private _createGhostArray(): Ghost[] {
        const ghosts: Ghost[] = [];
        this._resettingGhostStates = true;
        ghosts.push(new Blinky(this));
        ghosts.push(new Pinky(this));
        ghosts.push(new Inky(this));
        ghosts.push(new Clyde(this));
        this._resettingGhostStates = false;
        return ghosts;
    }

    drawBigDot(x: number, y: number) {
        const ms: number = this.playTime;
        if (ms < 0 || ms % 500 > 250) {
            const ctx: CanvasRenderingContext2D = this.canvas.getContext("2d");
            const sx: number = 135,
                sy: number = 38;
            const image: Image = this.assets.get("sprites");
            image.drawScaled2(ctx, sx, sy, 8, 8, x, y, 8, 8);
        }
    }

    drawFruit(ctx: CanvasRenderingContext2D) {
        if (this._fruitScoreIndex > -1) {
            this.paintPointsEarned(ctx, this._fruitScoreIndex, this._fruit.x, this._fruit.y);
            const time: number = this.playTime;
            if (time >= this._fruitScoreEndTime) {
                this._fruit = null;
                this._fruitScoreIndex = -1;
                this._fruitScoreEndTime = -1;
            }
        } else if (this._fruit) {
            this._fruit.paint();
        }
    }

    drawGhosts() {
        this._ghosts.forEach((ghost: Ghost) => {
            ghost.paint();
        });
    }

    drawScores(ctx: CanvasRenderingContext2D) {
        let scoreStr: string = this._score.toString();
        let x: number = 55 - scoreStr.length * 8;
        const y: number = 10;
        this.drawString(x, y, scoreStr, ctx);

        scoreStr = this._highScore.toString();
        x = 132 - scoreStr.length * 8;
        this.drawString(x, y, scoreStr, ctx);
    }

    drawScoresHeaders(ctx: CanvasRenderingContext2D) {
        this.drawString(16, 0, "1UP", ctx);
        this.drawString(67, 0, "HIGH SCORE", ctx);
    }

    drawSmallDot(x: number, y: number) {
        const ctx: CanvasRenderingContext2D = this.canvas.getContext("2d");
        ctx.fillRect(x, y, 2, 2);
    }

    drawSprite(dx: number, dy: number, sx: number, sy: number) {
        const image: Image = this.assets.get("sprites");
        const ctx: CanvasRenderingContext2D = this.canvas.getContext("2d");
        image.drawScaled2(ctx, sx, sy, 16, 16, dx, dy, 16, 16);
    }

    drawString(
        x: number,
        y: number,
        text: string | number,
        ctx: CanvasRenderingContext2D = this.canvas.getContext("2d")
    ) {
        const str: string = text.toString();
        const fontImage: SpriteSheet = this.assets.get("font");
        const alphaOffs: number = "A".charCodeAt(0);
        const numericOffs: number = "0".charCodeAt(0);
        let index: number;

        for (let i: number = 0; i < str.length; i++) {
            const ch: string = str[i];
            const chCharCode: number = str.charCodeAt(i);
            if (ch >= "A" && ch <= "Z") {
                index = fontImage.colCount + (chCharCode - alphaOffs);
            } else if (ch >= "0" && ch <= "9") {
                index = chCharCode - numericOffs;
            } else {
                switch (ch) {
                    case "-":
                        index = 10;
                        break;
                    case ".":
                        index = 11;
                        break;
                    case ">":
                        index = 12;
                        break;
                    case "@":
                        index = 13;
                        break;
                    case "!":
                        index = 14;
                        break;
                    default:
                        index = 15;
                        break;
                }
            }
            fontImage.drawByIndex(ctx, x, y, index);
            x += 9;
        }
    }

    getGhost(index: number): Ghost {
        return this._ghosts[index];
    }

    get stage(): number {
        return this._stage;
    }

    get hearts(): number {
        return this._hearts;
    }

    get penaltyBoxExitX(): number {
        return (this.getWidth() - 16) / 2;
    }

    get penaltyBoxExitY(): number {
        return 12 * 8 - 8 / 2;
    }

    ghostEaten(ghost: Ghost): number {
        switch (this._eatenGhostPointsIndex) {
            case 0:
                this._eatenGhostPointsIndex = 1;
                break;
            case 1:
                this._eatenGhostPointsIndex = 3;
                break;
            case 3:
                this._eatenGhostPointsIndex = 6;
                break;
            default:
            case 6:
                this._eatenGhostPointsIndex = 8;
                break;
        }
        this.increaseScore(this._extraPointsArray[this._eatenGhostPointsIndex]);

        this.audio.playSound(Sounds.GhostKill);
        return this._eatenGhostPointsIndex;
    }

    increaseLives(delta: number): number {
        return (this._hearts += delta);
    }

    increaseScore(delta: number) {
        this._score += delta;
        if (this._score > this._highScore) {
            this._highScore = this._score;
        }

        if (!this._earnedExtraLife && this._score >= GameRoot.extraLifeScore) {
            this.audio.playSound(Sounds.ExtraLife);
            this.increaseLives(1);
            this._earnedExtraLife = true;
        }
    }

    loadNextStage() {
        this.setLoopedAudio(null);
        this._stage++;
        this._fruit = null;
        this._fruitScoreIndex = -1;
        this._fruitScoreEndTime = -1;
        const state: GameScene = this.state as GameScene;
        state.reset();
    }

    makeGhostsBlue() {
        this._eatenGhostPointsIndex = 0;
        this._ghosts.forEach((ghost: Ghost) => {
            ghost.possiblyTurnBlue();
        });

        this.checkLoopedAudio();
    }

    paintPointsEarned(ctx: CanvasRenderingContext2D, ptsIndex: number, dx: number, dy: number) {
        const points: SpriteSheet = this.assets.get("points");
        points.drawByIndex(ctx, dx, dy, ptsIndex);
    }

    playChompAudio() {
        this.audio.playSound(this._chompAudio === 0 ? Sounds.Chomp1 : Sounds.Chomp2);
        this._chompAudio = (this._chompAudio + 1) % 2;
    }

    resetGhosts() {
        this._resettingGhostStates = true;

        const corners: Point[] = [
            new Point(2, 1),
            new Point(2, Level.horizontalTileCount - 2),
            new Point(Level.verticalTileCount - 2, 1),
            new Point(Level.verticalTileCount - 2, Level.horizontalTileCount - 2)
        ];
        const cornerSeed: number = Utils.randomInt(4);

        for (let i: number = 0; i < this._ghosts.length; i++) {
            this._ghosts[i].reset();
            this._ghosts[i].setCorner(corners[(cornerSeed + i) % 4]);
        }

        this._resettingGhostStates = false;
    }

    setLoopedAudio(audio: string) {
        if (audio !== this._loopedAudioName) {
            if (this._loopedAudioId != null) {
                this.audio.stopSound(this._loopedAudioId);
            }
            this._loopedAudioName = audio;
            this._loopedAudioId = audio != null ? this.audio.playSound(audio, true) : null;
        }
    }

    set ghostUpdateStrategy(strategy: GhostUpdateStrategy) {
        this._ghostUpdateStrategy = strategy;
    }

    startGame(stage: number) {
        this._hearts = 3;
        this._score = 0;
        this._stage = 0;

        const stagesData: any = this.assets.get("stages");
        const stageData: any = stagesData[stage];
        const levelState: any = new GameScene(stageData);
        this.setState(levelState);
    }

    startPacmanDying() {
        this.setLoopedAudio(null);
        this.audio.stopSound(Sounds.Death as any);
        this.pacman.startDying();
        this._fruit = null;
        this._fruitScoreIndex = -1;
        this._fruitScoreEndTime = -1;
    }

    updateSpriteFrames() {
        this.pacman.updateFrame();
        this._ghosts.forEach((ghost: Ghost) => {
            ghost.updateFrame();
        });
    }

    updateSpritePositions(level: Level, time: number) {
        switch (this._ghostUpdateStrategy) {
            case GhostUpdateStrategy.UpdateAll:
                this._ghosts.forEach((ghost: Ghost) => {
                    ghost.updatePosition(level, time);
                });
                break;
            case GhostUpdateStrategy.UpdateNone:
                break;
            case GhostUpdateStrategy.UpdateOne:
                this._ghosts[0].updatePosition(level, time);
                break;
        }

        this.pacman.updatePosition(level, time);
    }
}
