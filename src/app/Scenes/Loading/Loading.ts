import { Scene } from "../Scene";
import { Sounds } from "../../Sounds";
import { GameRoot } from "../../Game";
import { BaseStateArgs, FadeOutInState, Game, Utils } from "gtp";
import { MenuScene } from "../Menu/Menu";

export class LoadingScene extends Scene {
    private _assetsLoaded: boolean;
    constructor(args?: Game | BaseStateArgs) {
        super(args);
        this._assetsLoaded = false;
    }

    update(delta: number) {
        if (!this._assetsLoaded) {
            this._assetsLoaded = true;
            const game: Game = this.game;
            game.assets.onLoad(() => {
                game.assets.addSpriteSheet("font", "resources/font.png", 9, 7, 0, 0);
                game.assets.addImage("sprites", "resources/sprites.png", true);
                game.assets.addSpriteSheet("mapTiles", "resources/map.png", 8, 8, 0, 0);
                game.assets.addSpriteSheet("points", "resources/points.png", 18, 9, 0, 0);
                game.assets.addJson("stages", "resources/levelData.json");
                game.assets.addSound(Sounds.Chasing, "resources/sounds/chasing.wav");
                game.assets.addSound(Sounds.Chomp1, "resources/sounds/chomp_1.wav");
                game.assets.addSound(Sounds.Chomp2, "resources/sounds/chomp_2.wav");
                game.assets.addSound(Sounds.Death, "resources/sounds/death.wav");
                game.assets.addSound(Sounds.Fruit, "resources/sounds/fruit.wav");
                game.assets.addSound(Sounds.GhostKill, "resources/sounds/ghost_kill.wav");
                game.assets.addSound(Sounds.ExtraLife, "resources/sounds/extra_life.wav");
                game.assets.addSound(Sounds.RunningEyes, "resources/sounds/running_eyes.wav");
                game.assets.addSound(Sounds.Intermission, "resources/sounds/intermission.wav");
                game.assets.addSound(Sounds.GameStart, "resources/sounds/game_start.wav");
                game.assets.addSound(Sounds.Siren, "resources/sounds/siren.wav");
                game.assets.addSound(Sounds.Token, "resources/sounds/token.wav");
                game.assets.onLoad(() => {
                    const skipTitle: string = Utils.getRequestParam("skipTitle");
                    if (skipTitle !== null) {
                        const root: GameRoot = this.game as GameRoot;
                        root.startGame(0);
                    } else {
                        game.setState(new FadeOutInState(this, new MenuScene()));
                    }
                });
            });
        }
    }
}
