import { Scene } from "../Scene";
import { GameRoot } from "../../Game";
import { Pacman } from "../../Actors/Pacman/Pacman";
import { MenuScene } from "../Menu/Menu";
import { Sounds } from "../../Sounds";
import { Ghost, MovementState } from "../../Actors/Ghosts/Ghost";
import { InputManager } from "gtp";
import { Level } from "../../Level/Level";

declare let game: GameRoot;

enum InGameState {
    Ready,
    InGame,
    Dying,
    GameOver
}

export class GameScene extends Scene {
    private static readonly _dyingFrameDelay = 75;
    private static readonly _verticalTileCount = 32;
    private readonly _levelFile: number[][];
    private _level: Level;
    private _firstTimeThrough: boolean;
    private _updateScoreIndex: number;
    private _inGameState: InGameState;
    private _inGameStateStartTime: number;
    private _nextUpdateTime: number;
    private _nextDyingFrameTime: number;

    constructor(levelFile: number[][]) {
        super();
        this._levelFile = levelFile;
    }

    private get _readyDelay(): number {
        return this._firstTimeThrough ? 4500 : 2000;
    }

    enter() {
        game.pacman.reset();
        game.resetGhosts();

        this._level = new Level(this._levelFile);
        this._firstTimeThrough = true;
        this._updateScoreIndex = -1;

        this._inGameState = InGameState.Ready;
        this._firstTimeThrough = true;
        this._inGameStateStartTime = 0;
        this._nextDyingFrameTime = 0;
        this._nextUpdateTime = 0;
        this._lastSpriteFrameTime = 0;
    }

    private _paintExtraLives() {
        const hearts: number = game.hearts;
        if (hearts > 0) {
            let x: number = 24;
            const y: number = game.getHeight() - 2 * 8;
            const w: number = 2 * 8;
            for (let i: number = 0; i < hearts; i++) {
                game.drawSprite(x, y, 12 * 16, 3 * 16);
                x += w;
            }
        }
    }

    private _paintPossibleFruits() {
        const x: number = game.getWidth() - 12 - 2 * 8;
        const y: number = game.getHeight() - 2 * 8;

        switch (game.stage) {
            default:
            case 7:
                game.drawSprite(x - 112, y, 13 * 16, 3 * 16);
            case 6:
                game.drawSprite(x - 96, y, 13 * 16, 6 * 16);
            case 5:
                game.drawSprite(x - 80, y, 12 * 16, 6 * 16);
            case 4:
                game.drawSprite(x - 64, y, 13 * 16, 2 * 16);
            case 3:
                game.drawSprite(x - 48, y, 13 * 16, 5 * 16);
            case 2:
                game.drawSprite(x - 32, y, 12 * 16, 5 * 16);
            case 1:
                game.drawSprite(x - 16, y, 13 * 16, 4 * 16);
            case 0:
                game.drawSprite(x, y, 12 * 16, 4 * 16);
                break;
        }
    }

    render(ctx: CanvasRenderingContext2D) {
        super.render(ctx);
        this._level.render(ctx);
        const levelY: number = game.getHeight() - 2 * 8 - GameScene._verticalTileCount * 8;
        ctx.translate(0, levelY);

        game.drawFruit(ctx);

        const pacman: Pacman = game.pacman;
        if (this._updateScoreIndex === -1) {
            if (this._inGameState !== InGameState.GameOver) {
                pacman.render(ctx);
            }
        } else {
            const x: number = pacman.borders.x;
            const y: number = pacman.borders.y;
            game.paintPointsEarned(ctx, this._updateScoreIndex, x, y);
        }

        game.drawGhosts();
        ctx.translate(0, -levelY);

        game.drawScores(ctx);
        this._paintExtraLives();
        this._paintPossibleFruits();

        if (this._inGameState === InGameState.Ready) {
            const ready: string = "READY!";
            let x: number = (game.getWidth() - ready.length * 9) / 2;
            x += 3;
            game.drawString(x, 160, ready);
        } else if (this._inGameState === InGameState.GameOver) {
            const gameOver: string = "GAME OVER";
            const x: number = (game.getWidth() - gameOver.length * 9) / 2;
            game.drawString(x, 160, gameOver);
        }

        if (game.paused) {
            ctx.globalAlpha = 0.4;
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, game.getWidth(), game.getHeight());
            ctx.globalAlpha = 1;
            ctx.fillRect(50, 100, game.getWidth() - 100, game.getHeight() - 200);
            const paused: string = "PAUSED";
            const x: number = (game.getWidth() - paused.length * 9) / 2;
            game.drawString(x, (game.getHeight() - 18) / 2, paused);
        }
    }

    reset() {
        this._level.reset();
        game.resetPlayTime();
        game.pacman.reset();
        game.resetGhosts();
        this._inGameState = InGameState.Ready;
        this._inGameStateStartTime = 0;
        this._lastSpriteFrameTime = 0;
    }

    private _handleInput(delta: number, time: number) {
        const input: InputManager = game.inputManager;

        if (this._inGameState !== InGameState.GameOver && input.enter(true)) {
            game.paused = !game.paused;
            return;
        }

        if (!game.paused) {
            switch (this._inGameState) {
                case InGameState.InGame:
                    game.pacman.handleInput(this._level);
                    break;

                case InGameState.GameOver:
                    if (input.enter(true)) {
                        game.setState(new MenuScene(game));
                    }
                    break;
            }
        }
    }

    update(delta: number) {
        super.update(delta);
        this._handleInput(delta, game.playTime);
        const time: number = game.playTime;

        switch (this._inGameState) {
            case InGameState.Ready:
                if (this._firstTimeThrough && this._inGameStateStartTime === 0) {
                    this._inGameStateStartTime = time;
                    game.audio.playSound(Sounds.GameStart);
                }
                if (time >= this._inGameStateStartTime + this._readyDelay) {
                    this._inGameState = InGameState.InGame;
                    this._inGameStateStartTime = time;
                    game.resetPlayTime();
                    game.setLoopedAudio(Sounds.Siren);
                    this._firstTimeThrough = false;
                }
                break;

            case InGameState.InGame:
                this._updateInGameImpl(time);
                break;

            case InGameState.Dying:
                if (time >= this._nextDyingFrameTime) {
                    if (!game.pacman.incDying()) {
                        if (game.increaseLives(-1) <= 0) {
                            this._inGameState = InGameState.GameOver;
                        } else {
                            game.resetPlayTime();
                            game.pacman.reset();
                            game.resetGhosts();
                            this._inGameState = InGameState.Ready;
                            this._inGameStateStartTime = 0;
                            this._lastSpriteFrameTime = 0;
                        }
                    } else {
                        this._nextDyingFrameTime = time + GameScene._dyingFrameDelay;
                    }
                }
                break;

            case InGameState.GameOver:
                break;
        }
    }

    private _updateInGameImpl(time: number) {
        if (this._nextUpdateTime > 0 && time < this._nextUpdateTime) {
            return;
        }

        this._nextUpdateTime = 0;
        this._updateScoreIndex = -1;
        this._updateSpriteFrames();

        game.updateSpritePositions(this._level, time);
        const ghostHit: Ghost = game.checkForCollision();
        if (ghostHit) {
            switch (ghostHit.movmentState) {
                case MovementState.Blue:
                    this._nextUpdateTime = time + GameRoot.displayScoreLength;
                    ghostHit.movmentState = MovementState.Eyes;
                    this._updateScoreIndex = game.ghostEaten(ghostHit);
                    break;

                case MovementState.Eyes:
                case MovementState.EnteringBox:
                    // Do nothing
                    break;

                default:
                    game.startPacmanDying();
                    this._inGameState = InGameState.Dying;
                    this._nextDyingFrameTime = game.playTime + GameScene._dyingFrameDelay;
                    break;
            }
        }
    }
}
