import { Scene } from "../Scene";
import { GameRoot } from "../../Game";
import { Pacman } from "../../Actors/Pacman/Pacman";
import { Direction } from "../../Actors/Actor";
import { Ghost } from "../../Actors/Ghosts/Ghost";
import { BaseStateArgs, Game, Image, InputManager, SpriteSheet } from "gtp";

declare let game: GameRoot;

export class MenuScene extends Scene {
    private _lastKeypressTime: number;

    constructor(args?: GameRoot | BaseStateArgs) {
        super(args);
        this._initSprites();
        this.handleStart = this.handleStart.bind(this);
    }

    enter() {
        super.enter(game);

        game.canvas.addEventListener("touchstart", this.handleStart, false);
        this._lastKeypressTime = game.playTime;

        this._initSprites();
    }

    private _initSprites() {
        const pacman: Pacman = game.pacman;
        pacman.setLocation(game.getWidth() / 2, 240);
        pacman.direction = Direction.Left;
        const ghost: Ghost = game.getGhost(0);
        ghost.setLocation(game.getWidth() / 2 - 3 * 16, 240);
        ghost.direction = Direction.Left;
    }

    leaving(game: Game) {
        game.canvas.removeEventListener("touchstart", this.handleStart, false);
    }

    private getGame(): GameRoot {
        return this.game as GameRoot;
    }

    handleStart() {
        this._startGame();
    }

    render(ctx: CanvasRenderingContext2D) {
        const game: Game = this.game;
        game.clearScreen("rgb(0,0,0)");

        let temp: string = "PRESS ENTER";
        let charCount: number = temp.length - 1;
        let x = (game.getWidth() - 9 * charCount) / 2 - 10;
        let y = (game.getHeight() - 15 * 2) / 2;
        this.getGame().drawString(x, y, temp, ctx);
    }

    _stringWidth(str: string): number {
        const font: SpriteSheet = game.assets.get("font");
        return font.cellW * str.length;
    }

    _startGame() {
        game.startGame(0);
    }

    update(delta: number) {
        const playTime: number = game.playTime;
        if (playTime > this._lastKeypressTime + Scene.inputRepeatTime + 100) {
            const im: InputManager = game.inputManager;

            if (im.enter(true)) {
                this._startGame();
            }
        }
    }
}
