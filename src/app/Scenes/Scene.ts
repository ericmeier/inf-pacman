import { BaseStateArgs, Game, State } from "gtp";
import { GameRoot } from "../Game";

declare let game: GameRoot;

export class Scene extends State {
    protected _lastSpriteFrameTime: number;
    public static readonly inputRepeatTime = 200;

    constructor(args?: Game | BaseStateArgs) {
        super(args);
        this._lastSpriteFrameTime = 0;
    }

    protected _updateSpriteFrames() {
        const time: number = game.playTime;
        if (time >= this._lastSpriteFrameTime + 100) {
            this._lastSpriteFrameTime = time;
            game.updateSpriteFrames();
        }
    }
}
