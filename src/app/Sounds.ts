export enum Sounds {
    Chasing = "chasing",
    Chomp1 = "chomp1",
    Chomp2 = "chomp2",
    Death = "death",
    Fruit = "fruit",
    GhostKill = "ghostKill",
    ExtraLife = "extraLife",
    Intermission = "intermission",
    RunningEyes = "runningEyes",
    GameStart = "gameStart",
    Siren = "siren",
    Token = "token"
}
