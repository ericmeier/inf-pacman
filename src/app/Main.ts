import { GameRoot } from "./Game";
import { LoadingScene } from "./Scenes/Loading/Loading";

window.onload = () => {
    const win = window as any;
    win.game = new GameRoot({
        parent: document.getElementById("gameContainer"),
        width: 224,
        height: 288,
        keyRefresh: 300,
        assetRoot: "",
        targetFps: 60
    });
    win.game.setState(new LoadingScene());
    win.game.start();
};
