const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: "production",
    entry: [path.resolve("./src/app/Main.ts")],
    devtool: "source-map",
    output: {
        path: path.resolve("./build/web/"),
        filename: "[name].js"
    },
    resolve: {
        extensions: [".js", ".ts"],
        modules: ["src/app", "src/html", "src/css", "node_modules"]
    },
    plugins: [
        new CopyWebpackPlugin([{ from: "src/resources", to: "resources" }]),
        new HtmlWebpackPlugin({
            template: "src/index.html",
            inject: "body",
            hash: true
        }),
        new MiniCssExtractPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [{ loader: "ts-loader" }]
            },
            {
                test: /index.html$/,
                loader: "raw-loader"
            }
        ]
    },
    devServer: {
        contentBase: "./build/web"
    }
};
